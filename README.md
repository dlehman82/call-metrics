## Call Metrics SPA

I wrote this single-page application (SPA) in Laravel and VueJS.  It was originally written as a prototype back in July of 2021, but I have gone back and cleaned up code, added comments, and some basic
error handling.

## Highlights

- Laravel primarily for backend functionality (database connection and API)
- VueJS for the frontend display
- Straightforward responsive design
- Bootstrap CSS classes for the grid functionality and additional styling
- vuejs-smart-table to display the table data with sorting functionality
- vue-chartjs for the chart creation and display
- MySQL database to store the data
- npm package manager to install the 3rd party JS libraries and to compile the Vue JS code
- aristan for the model creation and database migration scripting
- eslint on the VueJS code

## Considerations / Potential Todos
- The API is not built out or structured since this app is not updating or deleting data, it is just a single request to the default route.
- The CSS is very simple, otherwise I would most likely use SASS
- Using JS alerts for error handling, I would use some kind of div based error notification in a production app.  The main idea was to show some basic error handling logic.

## Code Samples

- <a href="https://bitbucket.org/dlehman82/call-metrics/src/master/resources/js/pages/HomePage.vue">VueJS frontend display code</a> 
- <a href="https://bitbucket.org/dlehman82/call-metrics/src/master/app/Http/Controllers/API/MetricController.php">Backend API code</a>
- <a href="https://bitbucket.org/dlehman82/call-metrics/src/master/database/migrations/2021_07_02_173240_office_territory_table.php">Database insert migration script</a>

## Live Demo

- <a href="http://streetleveldoppler.com/temp/test/public/">http://streetleveldoppler.com/temp/test/public/</a>
