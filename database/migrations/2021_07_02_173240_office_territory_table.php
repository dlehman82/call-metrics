<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\DB;

class OfficeTerritoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('call_metrics', function (Blueprint $table) {
        $table->date('date');
        $table->integer('mail_sent');
        $table->decimal('cost', 9,2);
        $table->integer('calls');
        $table->integer('appointments');
        $table->integer('sales');
        $table->integer('revenue');
      });
            
      // import csv of data into call_metrics table
      $file = fopen('/app/data.csv', 'r');
      while (($line = fgetcsv($file)) !== FALSE) {
        $t = strtotime($line[0]);
                
        DB::table('call_metrics')->insert(
          [
            'date' => date('Y-m-d', $t),
            'mail_sent' => $line[1],
            'cost' => $line[2],
            'calls' => $line[3],
            'appointments' => $line[4],
            'sales' => $line[5],
            'revenue' => $line[6]
          ]
        );
      }
      fclose($file);
        
    }
   
      
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('call_metrics');
    }
}
